#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import sys
import argparse
from matplotlib.patches import Circle

"""File for determining whether some coordinates on Earth are on land or water.
"""

data = np.reshape(np.fromfile("data.bsq", dtype=np.uint8), [21600, 43200])


class SomeError(Exception):
    pass


def coordinates_to_indices(latitude, longitude):
    """Converts coordinates (latitude, longitude) in indices in our map.

    Args:
        latitude (float): latitude of the position.
        longitude (float): longitude of the position.

    Returns:
        (l,c) : indices in the table data for the same position.
    """
    if latitude == -90 :
        latitude = -89
    
    if longitude == 180 : 
        longitude = 179

    c = int(longitude * np.shape(data)[1] / 360 + np.shape(data)[1] / 2)
    l = int(-latitude * np.shape(data)[0] / 180 + np.shape(data)[0] / 2)
    return (l, c)


def land_or_water(latitude, longitude):
    """Given a set of coordinates (latitude, longitude) determines whether points to land or water on Earth.

    Args:
        latitude (float): latitude of the position.
        longitude (float): longitude of the position.

    Returns:
        str: "water" if on water, "land" otherwise
    """
    if latitude > 90.0 or latitude < -90.0 or longitude > 180.0 or longitude < -180.0:
        raise SomeError("test")
    l, c = coordinates_to_indices(latitude, longitude)
    if data[l, c] == 0:
        return "water"
    else:
        return "land"


def map_plot(latitude, longitude):
    """Plot function to plot the map and a patch on the map where the coordinates point.

    Args:
        latitude (float): latitude of the position.
        longitude (float): longitude of the position.
    """
    i, j = coordinates_to_indices(latitude, longitude)
    fig, ax = plt.subplots()
    circle = Circle((j, i), 300, color="white", label="Here you are")
    label = "Here you are"
    ax.text(
        j + 1000 / 2,
        i + 2000,
        label,
        ha="center",
        va="bottom",
        color="white",
        fontsize=8,
    )
    plt.imshow(data[::, ::])
    ax.add_patch(circle)
    ax.set_aspect("equal")
    plt.show()


def get_argument_from_user():
    """Argparser for land or water"""
    parser = argparse.ArgumentParser(
        "Land or Water",
        description="Given a set of coordinates, tells you whether you're on land or water",
    )
    parser.add_argument(
        "latitude", help="This is the latitude", type=float, default=0.0
    )
    parser.add_argument(
        "longitude", help="This is the longitude", type=float, default=0.0
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-v", "--verbose", action="store_true")
    # group.add_argument("-q", "--quiet", action="store_true")
    group.add_argument("-p", "--print", action="store_true")

    return parser.parse_args()


def main():
    args = get_argument_from_user()

    result = land_or_water(args.latitude, args.longitude)
    if args.verbose:
        if result == "land":
            print("You've got feet right on the ground")
        elif result == "water":
            print("You're drowning kid")

    elif args.print:
        if result == "land":
            print("You've got feet right on the ground")
        elif result == "water":
            print("You're drowning kid")
        map_plot(args.latitude, args.longitude)

    else:
        print(result)


if __name__ == "__main__":
    main()
