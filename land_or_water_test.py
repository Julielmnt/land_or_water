#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from land_or_water import land_or_water
import pytest


def test_lw_1():
    assert land_or_water(48.9,9.13)=='land'

def test_lw_ens():
    assert land_or_water(45.731123,4.826777) == 'land'

def test_lw_middle_pacific_ocean():
    assert land_or_water(-0.5,-134.0) == 'water'

def coordinates_not_in_allowed_interval():
    with pytest.raises(SomeError):
        land_or_water(180,-181)

@pytest.mark.parametrize("latitude_land, longitude_land", [
(48.9,9.13),
(45.731123,4.826777),
(60,87),
(77,-45),
(45,-106),
(6,23),
(-26,136),
(-90,-180),
(-90,180)])

def test_lw_on_land(latitude_land,longitude_land):
    assert land_or_water(latitude_land,longitude_land) == 'land'

@pytest.mark.parametrize("latitude_water, longitude_water", [
(-0.5,-134),
(-28.0,80),
(71,3),
(40,-39),
(-15,58),
(-60,44),
(90,-180),
(90,180)])


def test_lw_on_water(latitude_water,longitude_water):
    assert land_or_water(latitude_water,longitude_water) == 'water'